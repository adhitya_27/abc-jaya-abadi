<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBarangMasukTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('brg_msk')) {
            Schema::create('brg_msk', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->bigInteger('id_supplier');
                $table->dateTime('date', 0);
                $table->string('invoice_no');
                $table->timestamps();

                $table->foreign('id_supplier')
                        ->references('id')->on('suppliers')
                        ->onDelete('no action')
                        ->onUpdate('no action');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('brg_msk');
    }
}
