<?php

use Illuminate\Database\Seeder;
use App\User;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash;

class InitSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name' => 'admin',
            'email' => 'admin@mail.com',
            'password' => Hash::make('adminabc'),
            'level' => '1',
            'api_token' => Str::random(60),
        ]);
        User::create([
            'name' => 'manager',
            'email' => 'manager@mail.com',
            'password' => Hash::make('managerabc'),
            'level' => '2',
            'api_token' => Str::random(60),
        ]);
    }
}
