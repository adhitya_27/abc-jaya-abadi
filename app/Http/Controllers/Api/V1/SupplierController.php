<?php

namespace App\Http\Controllers\Api\V1;

use App\Supplier;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class SupplierController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Supplier::orderBy('id', 'asc')->paginate(10);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'nama' => 'required|max:100',
            'alamat' => 'required',
            'email' => 'required|email|unique:suppliers,email',
            'phone' => 'required|max:15'
        ]);

        return Supplier::create([
            'nama' => $request->nama,
            'alamat' => $request->alamat,
            'email' => $request->email,
            'phone' => $request->phone,
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return Supplier::findOrFail($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $supplier = Supplier::findOrFail($id);

        if ($request->has('nama')) {
            $request->validate([
                'nama' => 'required|max:100'
            ]);
            $supplier->nama = $request->nama;
        }

        if ($request->has('email')) {
            $request->validate([
                'email' => 'required|email|unique:suppliers,email,' . $supplier->id
            ]);
            $supplier->email = $request->email;
        }

        if ($request->has('phone')) {
            $request->validate([
                'phone' => 'required|max:15'
            ]);
            $supplier->phone = $request->phone;
        }

        if ($request->has('alamat')) {
            $request->validate([
                'alamat' => 'required'
            ]);
            $supplier->alamat = $request->alamat;
        }
        $supplier->save();

        return $supplier;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Supplier::destroy($id);

        return response()->json([
            'message' => 'success delete supplier data'
        ], 200);
    }
}
