<?php

namespace App\Http\Controllers\Api\V1;

use App\Karyawan;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class karyawanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Karyawan::orderBy('id', 'asc')->paginate(10);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'nama' => 'required|max:100',
            'alamat' => 'required',
            'email' => 'required|email|unique:karyawan,email',
            'phone' => 'required|max:15'
        ]);

        return Karyawan::create([
            'nama' => $request->nama,
            'alamat' => $request->alamat,
            'email' => $request->email,
            'phone' => $request->phone,
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return Karyawan::findOrFail($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $karyawan = Karyawan::findOrFail($id);

        if ($request->has('nama')) {
            $request->validate([
                'nama' => 'required|max:100'
            ]);
            $karyawan->nama = $request->nama;
        }

        if ($request->has('email')) {
            $request->validate([
                'email' => 'required|email|unique:karyawan,email,' . $karyawan->id
            ]);
            $karyawan->email = $request->email;
        }

        if ($request->has('phone')) {
            $request->validate([
                'phone' => 'required|max:15'
            ]);
            $karyawan->phone = $request->phone;
        }

        if ($request->has('alamat')) {
            $request->validate([
                'alamat' => 'required'
            ]);
            $karyawan->alamat = $request->alamat;
        }
        $karyawan->save();

        return $karyawan;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Karyawan::destroy($id);

        return response()->json([
            'message' => 'success delete karyawan data'
        ], 200);
    }
}
