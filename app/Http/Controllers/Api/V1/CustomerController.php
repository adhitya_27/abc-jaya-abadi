<?php

namespace App\Http\Controllers\Api\V1;

use App\Customer;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class CustomerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Customer::orderBy('id', 'asc')->paginate(10);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'company_name' => 'required|max:50',
            'alamat' => 'required',
            'email' => 'required|email|unique:customers,email',
            'phone' => 'required|max:15',
            'contact_person' => 'required|max:15'
        ]);

        return Customer::create([
            'company_name' => $request->company_name,
            'alamat' => $request->alamat,
            'email' => $request->email,
            'phone' => $request->phone,
            'contact_person' => $request->contact_person
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return Customer::findOrFail($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $customer = Customer::findOrFail($id);

        if ($request->has('company_name')) {
            $request->validate([
                'company_name' => 'required|max:100'
            ]);
            $customer->company_name = $request->company_name;
        }

        if ($request->has('email')) {
            $request->validate([
                'email' => 'required|email|unique:customers,email,' . $customer->id
            ]);
            $customer->email = $request->email;
        }

        if ($request->has('phone')) {
            $request->validate([
                'phone' => 'required|max:15'
            ]);
            $customer->phone = $request->phone;
        }

        if ($request->has('alamat')) {
            $request->validate([
                'alamat' => 'required'
            ]);
            $customer->alamat = $request->alamat;
        }

        if ($request->has('contact_person')) {
            $request->validate([
                'contact_person' => 'required'
            ]);
            $customer->contact_person = $request->contact_person;
        }
        $customer->save();

        return $customer;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Customer::destroy($id);

        return response()->json([
            'message' => 'success delete customer data'
        ], 200);
    }
}
